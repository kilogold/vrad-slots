using UnityEngine;
using System.Collections;

public class TextureAd : Ad
{
    public TextureAd(Texture adContentIn, Material adSurfaceMaterialIn, float lifetime) :
        base(adContentIn, adSurfaceMaterialIn, lifetime)
    {
    }

    public override void Play()
    {
        base.Play();
        adSurfaceMaterial.mainTexture = adContent;
    }

    public override void Stop()
    {
        base.Stop();
        adSurfaceMaterial.mainTexture = originalTexture;
    }
}
