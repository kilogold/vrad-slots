using UnityEngine;
using System.Collections;
using System;

public class Ad
{
    public enum AdType
    {
        Movie,
        Texture
    }

	//The Last Time
	
    /************ FIELDS **********************/

    /// <summary>
    /// Time before the ad expires.
    /// </summary>
    protected float curLifetime;
    protected float maxLifetime;

    protected Texture adContent;
    protected Texture originalTexture;
    protected Material adSurfaceMaterial;

    public event Action AdExpired;


    /************ PROPERTIES **********************/
    public bool IsPlaying { private set; get; }

    public bool IsExpired { get { return curLifetime <= 0; } }

    public Texture AdContent { get { return adContent; } }


    /************ METHODS **********************/
    public Ad(Texture adContentIn, Material adSurfaceMaterialIn, float lifetime)
    {
        adContent = adContentIn;
        adSurfaceMaterial = adSurfaceMaterialIn;
        originalTexture = adSurfaceMaterial.mainTexture;
        maxLifetime = curLifetime = lifetime;
    }

    public virtual void Play()
    {
        IsPlaying = true;
    }

    public virtual void Stop()
    {
        IsPlaying = false;
        curLifetime = maxLifetime;
    }

    public virtual void Pause()
    {
        IsPlaying = false;
    }

    public virtual void Tick(float deltaTime)
    {
        if (IsPlaying)
        {
            curLifetime -= deltaTime;

            if(IsExpired)
            {
                Pause();

                if(AdExpired != null)
                {
                    AdExpired();
                }
            }
        }
    }
}
