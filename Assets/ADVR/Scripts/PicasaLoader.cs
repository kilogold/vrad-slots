using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;

public class PicasaLoader : ContentLoader
{
    private string picasaUserAlbumId = "6155259190267990577";
    private string picasaUserId = "101867488510872520535";
    private string picasaGetAlbumUrl = string.Empty;

    // Temp placeholder to store loaded data to return from LoadContent.

    void Awake()
    {
        picasaGetAlbumUrl = "https://picasaweb.google.com/data/feed/api/user/" + picasaUserId + "/albumid/" + picasaUserAlbumId;
    }

    public override void LoadContent()
    {
        StartCoroutine(CoLoadContent());
    }

    IEnumerator CoLoadContent()
    {
        WWW request = new WWW(picasaGetAlbumUrl);

        yield return request;

        XmlDocument doc = new XmlDocument();
        
        doc.Load( new StringReader(request.text));
        request.Dispose();

        //Create an XmlNamespaceManager for resolving namespaces.
        XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
        nsmgr.AddNamespace("atom", "http://www.w3.org/2005/Atom");
        nsmgr.AddNamespace("openSearch", "http://a9.com/-/spec/opensearchrss/1.0/");
        nsmgr.AddNamespace("gphoto", "http://schemas.google.com/photos/2007");
        nsmgr.AddNamespace("exif", "http://schemas.google.com/photos/exif/2007");
        nsmgr.AddNamespace("media", "http://search.yahoo.com/mrss/");

        XmlNodeList nodeList = doc.DocumentElement.SelectNodes("//atom:content/@src", nsmgr);
        
        
        Debug.Log(nodeList.Count);

        Texture[] textureListLoadCache = new Texture2D[nodeList.Count];
        string[] textureUrlList = new string[nodeList.Count];

        for (int curNodeIndex = 0; curNodeIndex < nodeList.Count; curNodeIndex++)
        {
            Debug.Log(nodeList[curNodeIndex].InnerText);

            string textureUrl = nodeList[curNodeIndex].InnerText;
            WWW wwwTex = new WWW(textureUrl);
            
            yield return wwwTex;

            textureListLoadCache[curNodeIndex] = wwwTex.texture;
            textureUrlList[curNodeIndex] = textureUrl;

            wwwTex.Dispose();
        }

        RaiseContentLoadingCompleteEvent( new ContentLoadingCompleteEventArgs( textureListLoadCache, textureUrlList ) );
    }
}
