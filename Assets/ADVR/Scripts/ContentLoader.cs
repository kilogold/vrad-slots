using UnityEngine;
using System.Collections;
using System;

// Special EventArgs class to hold info about Shapes. 
public class ContentLoadingCompleteEventArgs : EventArgs
{
    public Texture[] textureList { get; private set;}
    public string[] stringUrlList { get; private set; }

    public ContentLoadingCompleteEventArgs(Texture[] textureListRef, string[] stringUrlListIn)
    {
        textureList = textureListRef;
        stringUrlList = stringUrlListIn;
    }
}

public abstract class ContentLoader : MonoBehaviour
{
    private IEnumerator CycleLoadContentCoroutineRef;

    /// <summary>
    /// Interval for the loader to redownload content from filehosting.
    /// </summary>
    [SerializeField]
    private float refetchIntervalSeconds;

    public event EventHandler<ContentLoadingCompleteEventArgs> ContentLoadingCompleteEvent;

    public abstract void LoadContent();

    protected void RaiseContentLoadingCompleteEvent(ContentLoadingCompleteEventArgs eventArgs)
    {
        if( ContentLoadingCompleteEvent != null)
        {
            ContentLoadingCompleteEvent(this, eventArgs);
        }
    }

    void Start()
    {
        CycleLoadContentCoroutineRef = CycleLoadContentCoroutine();

        // If we are not going to cycle, we want to fetch at least once...
        if (refetchIntervalSeconds == -1)
        {
            LoadContent();
        }
        else if (refetchIntervalSeconds > 0)
        {
            StartCoroutine(CycleLoadContentCoroutineRef);
        }
    }

    private IEnumerator CycleLoadContentCoroutine()
    {
        while (true)
        {
            LoadContent();
            yield return new WaitForSeconds(refetchIntervalSeconds);
        }
    }

    public void SetRefetchInterval( float interval )
    {
        if( interval == -1.0f )
        {
            StopCoroutine( CycleLoadContentCoroutineRef );
        }
        else if( interval > 0 && refetchIntervalSeconds == -1)
        {
            StartCoroutine( CycleLoadContentCoroutineRef );
        }
            
        refetchIntervalSeconds = interval;
    }
}
