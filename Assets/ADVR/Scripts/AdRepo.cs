using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

// Central location to cache all ad resources.
public class AdRepo : MonoBehaviour
{
    private class AdCacheContent
    {
        public Texture texture;
        public string textureUrl;

        public AdCacheContent( Texture textureIn, string textureUrlIn )
        {
            texture = textureIn;
            textureUrl = textureUrlIn;
        }
    }

    private List<AdCacheContent> textureAdCache = new List<AdCacheContent>();

    [SerializeField]
    private ContentLoader[] contentLoaders;

    /// <summary>
    /// Event to fire when the AdRepo has loaded all the initial
    /// content from the content loaders. At this point, the AdRepo
    /// is ready to provide ads to the surfaces.
    /// </summary>
    public event Action AdRepoInitialized;

    /// <summary>
    /// Flag to avoid re-initialization.
    /// </summary>
    private bool isAdRepoInitialized = false;

    /// <summary>
    /// Flag to keep track of sequential content loader initialization.
    /// Refer to Start().
    /// </summary>
    private bool isReadyToLoadContent = true;

    /// <summary>
    /// The count of how many content loaders have been initialized so far.
    /// </summary>
    private int contentLoaderInitCount;

    IEnumerator Start()
    {
        for (int curLoaderIndex = 0; curLoaderIndex < contentLoaders.Length;  )
        {
            // If we are already loading content...
            if( isReadyToLoadContent == false)
            {
                // Exit until the next frame
                yield return null;

                // Upon returning in the next frame, we start over.
                continue;
            }

            // At this point, we are ready to run the next content loader, so let's not allow any
            // other content loaders to execute until the current one is done.
            isReadyToLoadContent = false;

            // Content loading is performed after Start().
            GameObject contentLoaderInstance = Instantiate(contentLoaders[0].gameObject) as GameObject;
            contentLoaderInstance.transform.parent = transform;

            // Grab the instance's ContentLoader script component, and register
            // to the 'ContentLoadingCompleteEvent'.
            // When we get the callback from the event, we can destroy the contentloader game object
            // if we wish.
            ContentLoader contentLoaderScript = contentLoaderInstance.GetComponent<ContentLoader>();
            contentLoaderScript.ContentLoadingCompleteEvent += OnInternalContentLoadingCompleteEvent;

            // Manually increment the loop index
            ++curLoaderIndex;
        }
    }

    void OnInternalContentLoadingCompleteEvent(object sender, ContentLoadingCompleteEventArgs eventArgs)
    {
        // Unregister the event, we don't need it anymore.
        // ACTUALLY... We want to load more in the future... When DO we want to unregister? OnDestroy?
        //((ContentLoader)sender).ContentLoadingCompleteEvent -= OnInternalContentLoadingCompleteEvent;

        // Grab the loaded content
        int loadedTextureCount = 0;
        for (int curTextureIndex = 0; curTextureIndex < eventArgs.textureList.Length; curTextureIndex++)
        {
            // Attempt to find a duplicate via url
            AdCacheContent findResult = textureAdCache.Find(n => n.textureUrl == eventArgs.stringUrlList[curTextureIndex] );

            // Only add non-duplicate entries.
            if (findResult == null)
            {
                AdCacheContent newEntry = new AdCacheContent(eventArgs.textureList[curTextureIndex], eventArgs.stringUrlList[curTextureIndex]);
                textureAdCache.Add(newEntry);
                loadedTextureCount++;
            }
        }

        if (loadedTextureCount > 0)
        {
            Debug.Log("<color=lime>" + loadedTextureCount + " new textures loaded!</color>");
        }

        isReadyToLoadContent = true;
        ++contentLoaderInitCount;

        // If we've initialized all content loaders...
        // Should only happen once per repo init.
        if (contentLoaderInitCount >= contentLoaders.Length &&
            AdRepoInitialized != null && isAdRepoInitialized == false )
        {
            AdRepoInitialized();
            isAdRepoInitialized = true;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="duplicateCheck">The item to check </param>
    /// <returns></returns>
    private Texture GetRandomTextureAd( Texture duplicateCheck = null )
    {
        Texture returnTexture = null;

        if (null != textureAdCache && textureAdCache.Count > 0)
        {
            do
            {
                int listIndex = UnityEngine.Random.Range(0, textureAdCache.Count);
                returnTexture = textureAdCache[listIndex].texture;
            }
            while (duplicateCheck == returnTexture);
        }

        return returnTexture;
    }

    public TextureAd GenerateTextureAd(Material adSurfaceMaterial, float lifetime)
    {
        Texture newAdTexture = GetRandomTextureAd();

        if (null != newAdTexture)
        {
            TextureAd newAd = new TextureAd(newAdTexture, adSurfaceMaterial, lifetime);

            return newAd;
        }
        else
        {
            return null;
        }
    }

    /*************************************************************
     * MOVIE TEXTURE CODE
     *************************************************************/

    //[SerializeField]
    //private MovieTexture[] movieList;

    //private MovieTexture GetRandomMovieTextureAd(MovieTexture duplicateCheck = null)
    //{
    //    MovieTexture returnTexture = null;

    //    if (null != movieList)
    //    {
    //        do
    //        {
    //            int listIndex = UnityEngine.Random.Range(0, movieList.Length);
    //            returnTexture = Instantiate(movieList[listIndex]) as MovieTexture;
    //        }
    //        while (duplicateCheck == returnTexture);
    //    }

    //    return returnTexture;
    //}

    //public MovieAd GenerateMovieAd(Material adSurfaceMaterial, float lifetime, bool isLooping)
    //{
    //    MovieTexture newAdTexture = GetRandomMovieTextureAd();
    //    if (null != newAdTexture)
    //    {
    //        MovieAd newAd = new MovieAd(newAdTexture, adSurfaceMaterial, lifetime, isLooping);

    //        return newAd;
    //    }
    //    else
    //    {
    //        return null;
    //    }
    //}
}
