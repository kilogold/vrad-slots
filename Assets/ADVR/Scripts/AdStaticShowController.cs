using UnityEngine;
using System.Collections;

public class AdStaticShowController : MonoBehaviour
{
    private Ad displayedAd;

    private AdRepo model;

    /// <summary>
    /// The renderer where the ad will be placed.
    /// This is also known as the ad surface.
    /// </summary>
    [SerializeField]
    private Renderer view;

    [SerializeField]
    Ad.AdType adType;

    // Use this for initialization
    IEnumerator Start()
    {
        model = GameObject.FindObjectOfType<AdRepo>();

        while (null == displayedAd)
        {
            switch (adType)
            {
                //case Ad.AdType.Movie:
                //    displayedAd = model.GenerateMovieAd(view.material, 6000.0f, true);
                //    break;

                case Ad.AdType.Texture:
                    displayedAd = model.GenerateTextureAd(view.material, 100.0f);
                    break;

                default:
                    break;
            }

            yield return null;
        }

        displayedAd.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (null != displayedAd)
        {
            displayedAd.Tick(Time.deltaTime);
        }
    }
}
