using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AdSequenceShowController : MonoBehaviour
{
    [SerializeField]
    private List<Ad> displayedAds;

    private AdRepo model;

    /// <summary>
    /// The renderer where the ad will be placed.
    /// This is also known as the ad surface.
    /// </summary>
    [SerializeField]
    private Renderer view;

    [SerializeField, Tooltip("How many ads to display in the sequence")]
    private int sequenceLength;

    /// <summary>
    /// The ad in the sequence that is currently being displayed.
    /// </summary>
    private int curSequenceAdIndex;

    [SerializeField]
    private bool isLooping;

    [SerializeField]
    Ad.AdType adType;

    [SerializeField]
    private float AdCycleLifetime;

    // Use this for initialization
    IEnumerator Start()
    {
        model = GameObject.FindObjectOfType<AdRepo>();
        displayedAds = new List<Ad>(sequenceLength);

        for (int curAdIndx = 0; curAdIndx < sequenceLength; )
        {
            switch (adType)
            {
                //case Ad.AdType.Movie:
                //    {
                //        MovieAd movieAd = model.GenerateMovieAd(view.material, 5.0f, false);
                //        if (null != movieAd)
                //        {
                //            displayedAds.Add(movieAd);
                //            curAdIndx++;
                //        }
                //    }
                //    break;

                case Ad.AdType.Texture:
                    {
                        TextureAd textureAd = model.GenerateTextureAd(view.material, AdCycleLifetime);

                        if (null != textureAd)
                        {
                            displayedAds.Add(textureAd);
                            curAdIndx++;
                        }
                    }
                    break;
                default:
                    break;
            }

            yield return null;
        }

        curSequenceAdIndex = 0;
        displayedAds[curSequenceAdIndex].AdExpired += OnAdExpired;
        displayedAds[curSequenceAdIndex].Play();
    }

    void OnAdExpired()
    {
        displayedAds[curSequenceAdIndex].Stop();
        displayedAds[curSequenceAdIndex].AdExpired -= OnAdExpired;

        curSequenceAdIndex++;
        if (curSequenceAdIndex >= sequenceLength)
        {
            if (isLooping == false)
                return;


            curSequenceAdIndex = 0;
        }

        displayedAds[curSequenceAdIndex].AdExpired += OnAdExpired;
        displayedAds[curSequenceAdIndex].Play();
    }

    // Update is called once per frame
    void Update()
    {
        // Only false when we are not looping.
        // Consider set architecture that implies this.
        if (curSequenceAdIndex < displayedAds.Count)
        {
            displayedAds[curSequenceAdIndex].Tick(Time.deltaTime);
        }
    }
}
