using UnityEngine;
using System.Collections;
//Attach this Script to the speed-Tansforms: speed_1 , speed_2 and speed_3 
public class SlotSpeedFlow : MonoBehaviour 
{
	// UV Animation rate x or/and y
	public Vector2 uvAnim = new Vector2 (0.0f, 10.0f);
	
	string tex = "_MainTex"; 
	Vector2 uvCurrent = Vector2.zero;
	Renderer ren;
	GameObject obj;

	
	void OnEnable ()
	{
		ren = renderer;
		obj = gameObject;
	}
	
	void LateUpdate ()
	{
		if (obj.active) 
		{
			//Set uv coord to current uv multiplied with time
			uvCurrent += (uvAnim * Time.deltaTime);
			// Assign texture and previous uv offset animation
			ren.sharedMaterial.SetTextureOffset (tex, uvCurrent);
		}
	}
}
