using UnityEngine;
using System.Collections;

public class SlotMachineProgram : MonoBehaviour 
{
	public string resourcesFolderName = "Slot_Icons";
	public Texture[] icons;
	public string startButton = "q";
	public string stopButton = "e";
	
    public float slotSpeed = 12.0f;
	public float minSpinSeconds = 1.5f;
	public float maxSpinSeconds = 4.0f;
	
	int index;
	
	Material mat1;
	Material mat2;
	Material mat3;

	Transform r1;
	Transform r2;
	Transform r3;
	
	Transform speed1;
	Transform speed2;
	Transform speed3;
	
	Transform button1;
	Transform button2;
	Transform button3;
	bool stop = false;
	
	Transform nrBoard;
	Transform nr1;
	Transform nr2;
	Transform nr3;
	Transform nr4;
	bool winBoardRuns = false;
	
	Transform fLight;
	bool flashLightTurning = false;
	
	Transform arrowBoard;
	
	Transform backG;
	public bool useBackBlendColor = true;
	public Color secFadeColor = Color.magenta;
	Color backRefCol;
	Color backMainCol;
	public float colorBlendSpeed = 1.0f;
	Color back_Col1 = Color.white;
	Color back_Col2 = Color.grey;
	bool colorCoroutine = false;
	
	public AudioClip leverPullClip;
	public AudioClip buttonPress1Clip;
	public AudioClip buttonPress2Clip;
	public AudioClip rollStopClip;
	public AudioClip lose1Clip;
	public AudioClip lose2Clip;
	public AudioClip freeGameClip;
	public AudioClip winClip;
	public AudioClip resetMachineClip;
	
    bool running1 = false;
	bool running2 = false;
	bool running3 = false;
	bool machineRuns = false;
	bool _jackPot = false;
	
	
	void Start ()
	{
		// Get transform children
		foreach (Transform child in transform)
		{
			if (child.name == "arrow_board")
				arrowBoard = child;
			if (child.name == "button_1")
				button1 = child;
			if (child.name == "button_2")
				button2 = child;
			if (child.name == "button_3")
				button3 = child;
			if (child.name == "flashLight")
				fLight = child;
			if (child.name == "nr_1")
				nr1 = child;
			if (child.name == "nr_2")
				nr2 = child;
			if (child.name == "nr_3")
				nr3 = child;
			if (child.name == "nr_4")
				nr4 = child;
			if (child.name == "nr_board")
				nrBoard = child;
			if (child.name == "roll_1")
				r1 = child;
			if (child.name == "roll_2")
				r2 = child;
			if (child.name == "roll_3")
				r3 = child;
			if (child.name == "slotM_back")
				backG = child;
			if (child.name == "speed_1")
				speed1 = child;
			if (child.name == "speed_2")
				speed2 = child;
			if (child.name == "speed_3")
				speed3 = child;
		}
		// Load all images from our folder
		Object[] texs = Resources.LoadAll (resourcesFolderName);
		icons = new Texture[texs.Length];
		// Assign images to Texture Array
		for (int i = 0; i < texs.Length; i++) 
		{
			icons[i] = (Texture)texs[i];
		}
		// Get Slot Materials
		mat1 = r1.renderer.material;
		mat2 = r2.renderer.material;
		mat3 = r3.renderer.material;
		
		index = new int ();
		
		nrBoard.gameObject.active = false;
		fLight.renderer.material.SetColor ("_Color", Color.black);
		speed1.gameObject.active = false;
		speed2.gameObject.active = false;
		speed3.gameObject.active = false;
	}
	void LateUpdate ()
	{
		// Do nothing if no Icons available
		if (icons.Length == 0)
			return;
		// Blend background colors
		if (useBackBlendColor)
		{
			if (!colorCoroutine) 
			{
				StartCoroutine (BackColorBlend ());
			}
			backMainCol = Color.Lerp (backMainCol, backRefCol, Time.deltaTime * colorBlendSpeed);
			backG.renderer.material.color = backMainCol;
		}
		
		// START the machine
        float axisV = Input.GetAxis("Vertical");
        if (axisV < 0 && !machineRuns)
		{
			StartCoroutine (Luck ());
			animation.Play ();
		}
		// Cycle Icons
	    else if (running3)
		{
			index = (int)(Time.deltaTime + slotSpeed);
			index = index % icons.Length;
			mat3.mainTexture = icons[index];
			slotSpeed++;
			// Button3 Flashing
			if (Time.time % 1.9f < 1)   // (Time.time % 2 < 1)
			{
				button3.gameObject.active = false;
			}	
			else button3.gameObject.active = true;
			if (running2)
			{
				// Button2 Flashing
				if (Time.time % 1.7f < 1)
				{
					button2.gameObject.active = false;
				}	
				else button2.gameObject.active = true;
				mat2.mainTexture = icons[index];
				if (running1)
				{
					// Button1 Flashing
					if (Time.time % 1.6f < 1)
					{
						button1.gameObject.active = false;
					}	
					else button1.gameObject.active = true;
					mat1.mainTexture = icons[index];
				}
			}
			// Limit Speed
			if (slotSpeed > 1000.0f)
				slotSpeed = 100;
		}
		// let the flashlight spin, Free Game or Jackpot!!
		if (flashLightTurning)
		{
			fLight.Rotate (Vector3.up * (600 * Time.deltaTime));
			
			if (_jackPot)
			{
				// NrBoard flashing
				if (Time.time % 2 < 1)
					nrBoard.gameObject.active = true;
				else 
					nrBoard.gameObject.active = false;
				// Buttons flashing
				if (Time.time % 1.7f < 1)
				{
					button1.gameObject.active = false;
					button2.gameObject.active = false;
					button3.gameObject.active = false;
				}	
				else
				{
					button1.gameObject.active = true;
					button2.gameObject.active = true;
					button3.gameObject.active = true;
				}
			}
		}
		// Flashing Arrow
		if (Time.time % 2 < 1 && !machineRuns)
			arrowBoard.gameObject.active = true;
		else arrowBoard.gameObject.active = false;	
	    // STOP each Slot
		if (Input.GetKeyDown (stopButton) && machineRuns)
		{
			if (running1) 
			{
				stop = true;
				running1 = false;
				speed1.gameObject.active = false;
				button1.gameObject.active = true;
				// Audio
				if (buttonPress1Clip != null)  
				{
			    	audio.clip = buttonPress1Clip;
			    	audio.Play ();
			    }
			}
			else if (running2)
			{
				stop = true;
				running2 = false;
				speed2.gameObject.active = false;
				button2.gameObject.active = true;
				// Audio
				if (buttonPress2Clip != null)  
				{
			    	audio.clip = buttonPress2Clip;
			    	audio.Play ();
			    }
			}
			else if (running3)
			{
				stop = true;
				running3 = false;
				speed3.gameObject.active = false;
				button3.gameObject.active = true;
				// 3rd row: NrBoard Off
				nrBoard.gameObject.active = false;
				// Audio
				audio.clip = buttonPress1Clip;
			    audio.Play ();
			}
		}
	}
	// Timing/Random Coroutine
	IEnumerator Luck ()
	{
		machineRuns = true;
		// Audio
		if (leverPullClip != null) 
		{
			audio.clip = leverPullClip;
			audio.Play ();
		}
		if (!winBoardRuns)
			StartCoroutine ("WinBoard");
		running1 = true;
		running2 = true;
		running3 = true;
		speed1.gameObject.active = true;
		speed2.gameObject.active = true;
		speed3.gameObject.active = true;
		nrBoard.gameObject.active = true;
		// if not pressed the stop button
		if (!stop) 
		{
			yield return new WaitForSeconds (Random.Range (minSpinSeconds, maxSpinSeconds));
		}
		else
			yield return new WaitForSeconds (0.4f);
		speed1.gameObject.active = false;
		button1.gameObject.active = true;
		running1 = false;
		// Audio
		if (rollStopClip != null)
		{
			audio.clip = rollStopClip;
			audio.Play ();
		}
		if (!stop) 
		{
			yield return new WaitForSeconds (Random.Range (minSpinSeconds, maxSpinSeconds));
		}
		else
			yield return new WaitForSeconds (0.4f);
		speed2.gameObject.active = false;
		button2.gameObject.active = true;
		running2 = false;
		// Audio
		if (rollStopClip != null)
		{
			audio.clip = rollStopClip;
			audio.Play ();
		}
		if (!stop) 
		{
			yield return new WaitForSeconds (Random.Range (minSpinSeconds, maxSpinSeconds));
		}
		else
			yield return new WaitForSeconds (0.4f);
		// Row End
		stop = false;
		speed3.gameObject.active = false;
		button3.gameObject.active = true;
		running3 = false;
		// Audio
		if (rollStopClip != null)
		{
			audio.clip = rollStopClip;
			audio.Play ();
		}
		if (mat1.mainTexture.name == mat2.mainTexture.name) 
		{
			// First 2 .. Double Strike!
			StartCoroutine (FreeGame ());
			
			if (mat2.mainTexture.name == mat3.mainTexture.name) 
			{
				// All 3 .. Jackpot!!!
				StartCoroutine (Jackpot ());
			}
		}
		else
			// Insert Coin.. next try
			StartCoroutine (NoLuck ());
		
		nrBoard.gameObject.active = false;
		machineRuns = false;                     
	}
	// You got a Triple Strike !!!
	IEnumerator Jackpot ()
	{
		_jackPot = true;
		//machineRuns = true;
		flashLightTurning = true;
		fLight.renderer.material.SetColor ("_Color", Color.white);
		StopCoroutine ("WinBoard");
		winBoardRuns = false;
		// Audio
		if (winClip != null)
		{
			audio.clip = winClip;
			audio.Play ();
		}
		
		if (mat1.mainTexture == icons[0])
			nrBoard.position = nr2.position; // Melon
		if (mat1.mainTexture == icons[1])
			nrBoard.position = nr4.position; // Limette	
		if (mat1.mainTexture == icons[2])
			nrBoard.position = nr1.position; // Wine grapes
		if (mat1.mainTexture == icons[3])
			nrBoard.position = nr4.position; // Bell			
		if (mat1.mainTexture == icons[4])
			nrBoard.position = nr1.position; // Cherry
		if (mat1.mainTexture == icons[5])
			nrBoard.position = nr3.position; // Orange
		if (mat1.mainTexture == icons[6])
			nrBoard.position = nr2.position; // Mighty 7
		if (mat1.mainTexture == icons[7])
			nrBoard.position = nr3.position; // BAR
		if (mat1.mainTexture == icons[8])
			nrBoard.position = nr4.position; // Plum
		// Enjoy for a moment 
		yield return new WaitForSeconds (4.5f);
		fLight.renderer.material.SetColor ("_Color", Color.black);
		flashLightTurning = false;
		//machineRuns = false;
		_jackPot = false;
	}
	// You got a Double Strike, 1 Free game !! (roll1 and roll2)
	IEnumerator FreeGame ()
	{
		if (!_jackPot) 
		{
			//machineRuns = true;
			flashLightTurning = true;
			fLight.renderer.material.SetColor ("_Color", Color.white);
			// Audio
			if (freeGameClip != null)
			{
				audio.clip = freeGameClip;
				audio.Play ();
			}
			yield return new WaitForSeconds (2.5f);
			fLight.renderer.material.SetColor ("_Color", Color.black);
			flashLightTurning = false;
			//machineRuns = false;
		}
	}
	// Nothing, next try !
	IEnumerator NoLuck ()
	{
		//machineRuns = true;
		// Audio
		if (lose1Clip != null && lose2Clip != null)
		{
			if (Time.time % 2 < 1) 
			{
				audio.clip = lose1Clip;
			}
			else
				audio.clip = lose2Clip;
			audio.Play ();
		}
		yield return new WaitForSeconds (0.3f);
		//machineRuns = false;
	}
	// Color blending
	IEnumerator BackColorBlend ()
	{
		colorCoroutine = true;
		back_Col2 = secFadeColor;
		backRefCol = Color.Lerp (back_Col1, back_Col2, Random.value);
		yield return new WaitForSeconds (Random.Range (1.5f, 2.5f));
		back_Col2 = Color.white;
		backRefCol = Color.Lerp (back_Col1, back_Col2, Random.value);
		yield return new WaitForSeconds (Random.Range (1.5f, 2.5f));
		colorCoroutine = false;
	}
	// WinBoard movement
	IEnumerator WinBoard ()
	{
		while (!_jackPot && machineRuns) 
		{
			winBoardRuns = true;
			nrBoard.position = nr4.position;
			yield return new WaitForSeconds (0.3f);
			nrBoard.position = nr3.position;
			yield return new WaitForSeconds (0.3f);
			nrBoard.position = nr2.position;
			yield return new WaitForSeconds (0.3f);
			nrBoard.position = nr1.position;
			yield return new WaitForSeconds (0.3f);
			winBoardRuns = false;
		}
	}
}
