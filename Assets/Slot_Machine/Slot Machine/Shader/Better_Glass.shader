Shader "Better_Glass" {
    Properties {
        _Color ("Main Color", Color) = (1,1,1,1)
        _SpecColor ("Spec Color", Color) = (1,1,1,1)
        _Emission ("Emmisive Color", Color) = (0,0,0,0)
        _Shininess ("Shininess", Range (0.01, 1)) = 0.7

        
        _MainTex ("Base (RGB) Transparency (A)", 2D) = "white" {}
        _Reflections ("Base (RGB) Gloss (A)", Cube) = "skybox" { TexGen CubeReflect }
        
    }
    SubShader {
        Tags { "Queue" = "Transparent" }
        Pass {
            Blend SrcAlpha OneMinusSrcAlpha
            Material {
                Diffuse [_Color]
                Ambient [_Color]
                Shininess [_Shininess]
                Specular [_SpecColor]
                Emission [_Emission]
            }
            Lighting On
            //SeparateSpecular On

            SetTexture [_MainTex] {
                combine texture * primary double, texture * primary
            }
        }
        Pass {
            Blend One One
            Material {
                Diffuse [_Color]
                //Ambient [_Color]
                //Shininess [_Shininess]
                //Specular [_SpecColor]
                //Emission [_Emission]
            }
            Lighting On
            //SeparateSpecular On
            SetTexture [_Reflections] {
                combine texture
                Matrix [_Reflection]
            }
        }
    }
}